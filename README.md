Approach:
To build a chatroom i need bidirectional connection between client and server -> thinking websocket.
Searching for basic java websocket examples yields javax.websocket-api and spring STOMP
Since the requirement is also to have an admin page to delete and create room -> thinking some java MVC framework
to collect web inputs and render responses

I don't want to spent too much time setting up dev env (web server, database, ...) so i need some framework and a base
code.

->copy base code from:
https://www.callicoder.com/spring-boot-websocket-chat-example/
https://spring.io/guides/gs/messaging-stomp-websocket/#scratch
then adding spring MVC to fill in the create/destroy room feature.
Also add spring boot security to only "allow a set of fixed users" to use the app

Architecture:
This project has 2 parts:
1.The serverside is java app built on springboot to
 .handle http requests to create/delete room
 .receive and route STOMP messages from web client through websocket to another web client
2.The client side is a javascript application running on browser to send STOMP request to server and render STOMP
message on browser

Implementation detail:
To only "allow a set of fixed users" to use the app, user must be registered in advance and they need to login to use
the app
Registering user is not in the scope of this demo, thus, I am using a mock user database (defined in
WebSecurityConfig.java)
The collection of chat room is kept in memory in a Map
A user can be present in multiple room at the same time


Application flow
1.An admin must login and use localhost:8080/admin page to create a chatroom for authenticated users to use
 there is no special security configuration for /admin page yet thus, any authenticated user can access /admin
2.A user accesses the app at localhost:8080/ and be prompted for credential
3.Upon successfully authenticated, the user is asked to select a chatroom and pick a nickname
4.upon selecting chatroom and nickname, user is connected to the chat room if it exist, the nickname may has been
 auto-assigned if the selected one conflict with another user in the room
5.to broadcast message to the whole room, user enter msg and leave the "to" field blank
6.to send private message to another user in the room he must include the nickname of the other user in the "to" field

To be improved:

Server side:
1.persist user account, room and room member on disk so that upon app restart, the they are not lost
2.use oauth or saml to secure app to avoid keeping user credential on service provider (chat)
3.user is currently added to the room before websocket is connected, if something wrong happen before ws connected
 (browser crash for exampl) then there is no websocket event to remove the user from the room

Client side (this demo do not focus on client implementation)
1.list all users available in room
2.click to select user to send private message
3.also show private messages on the sender page
4.add button to leave room which bring user back to room selection page which effectively close ws and thus trigger
 LEAVE

